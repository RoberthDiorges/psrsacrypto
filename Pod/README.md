# PSRSACrypto

[![CI Status](https://img.shields.io/travis/PSRoberthDiorges/PSRSACrypto.svg?style=flat)](https://travis-ci.org/PSRoberthDiorges/PSRSACrypto)
[![Version](https://img.shields.io/cocoapods/v/PSRSACrypto.svg?style=flat)](https://cocoapods.org/pods/PSRSACrypto)
[![License](https://img.shields.io/cocoapods/l/PSRSACrypto.svg?style=flat)](https://cocoapods.org/pods/PSRSACrypto)
[![Platform](https://img.shields.io/cocoapods/p/PSRSACrypto.svg?style=flat)](https://cocoapods.org/pods/PSRSACrypto)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

PSRSACrypto is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'PSRSACrypto'
```

## Author

PSRoberthDiorges, tqi_rdiorges@uolinc.com

## License

PSRSACrypto is available under the MIT license. See the LICENSE file for more info.

//
//  KeychainUtil.h
//  CarteiraPagamento
//
//  Created by Ricardo Borelli
//  Copyright (c) 2016 PagSeguro. All rights reserved.
//

#import <Foundation/Foundation.h>


//needs: Security.framework

@interface KeychainUtil : NSObject {
    
}

+ (BOOL)saveGenericPasswd:(NSData*)data forAccount:(NSString*)account service:(NSString*)service passwdKey:(NSString*)key overwrite:(BOOL)overwrite;

+ (BOOL)updateGenericPasswd:(NSData*)data forAccount:(NSString*)account service:(NSString*)service passwdKey:(NSString*)key;

+ (NSString*)loadPasswdStringForAccount:(NSString*)account service:(NSString*)service passwdKey:(NSString*)key;

+ (NSData*)loadPasswdDataForAccount:(NSString*)account service:(NSString*)service passwdKey:(NSString*)key;

+ (BOOL)deleteGenericPasswdForAccount:(NSString*)account service:(NSString*)service passwdKey:(NSString*)key;

+ (BOOL)genericPasswdExistsForAccount:(NSString*)account service:(NSString*)service passwdKey:(NSString*)key;


+ (NSData*)dataFromDictionary:(NSMutableDictionary*)dic;

+ (NSMutableDictionary*)dictionaryFromData:(NSData*)data;


+ (NSString*)fetchStatus:(OSStatus)status;


+ (void)resetCredentials;

+ (void)dumpCredentials;

@end

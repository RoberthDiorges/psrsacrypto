//
//  RSA.h
//  CarteiraPagamento
//
//  Created by Ricardo Borelli
//  Copyright (c) 2016 PagSeguro. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface RSA : NSObject
{
    
}
/*!
 @method encriptarRSAValor:comMod:comExpoente:
 @abstract Encripta a string da variável 'valor' usando o 'modulus' e o 'expoente'
 @return Retorna uma string ja criptografada com os parametros de entrada
 */
+ (NSString *)encriptarRSAValor:(NSString *)valor comMod:(NSString *)modulus comExpoente:(NSString *)expoente;

/*!
 @method hexval:
 @abstract Função que transforma um NSData em um NSString
 @return Retorna uma string com valores em hexadecimal
 */
+ (NSString *)hexval:(NSData *)data;

@end

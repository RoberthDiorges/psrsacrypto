//
//  RSA.m
//  CarteiraPagamento
//
//  Created by Ricardo Borelli
//  Copyright (c) 2016 PagSeguro. All rights reserved.
//


#import "RSA.h"
#import "CryptoUtil.h"
#import "RSAFunctions.h"

@implementation RSA

/*!
 @method encriptarRSAValor:comMod:comExpoente:
 @abstract Encripta a string da variável 'valor' usando o 'modulus' e o 'expoente'
 @return Retorna uma string ja criptografada com os parametros de entrada
 */
+ (NSString *)encriptarRSAValor:(NSString *)valor comMod:(NSString *)modulus comExpoente:(NSString *)expoente
{
    NSData *expData = nil;
    if(expoente)
    {
        expData = [self stringToData:expoente];
    }
    
    NSData *publicKeyData = [CryptoUtil generateRSAPublicKeyWithModulus:[self stringToData:modulus] exponent:expData];
    
    [CryptoUtil saveRSAPublicKey:publicKeyData appTag:@"RSA" overwrite:YES];
    
    SecKeyRef publicKey = [CryptoUtil loadRSAPublicKeyRefWithAppTag:@"RSA"];
    
    NSData *data = [CryptoUtil encryptString:valor RSAPublicKey:publicKey padding:kSecPaddingPKCS1];
    
    NSString *resultado = [self hexval:data];
    
    return resultado;
}

/*!
 @method stringToData:
 @abstract Transforma uma string em um NSData
 @return Retorna um NSData
 */
+ (NSData *)stringToData:(NSString *)valor
{
    NSString *r = [RSAFunctions convertToBinary:valor];
    
    NSMutableString *zero = [[NSMutableString alloc] init];
    
    //se o resto for 0 não precisa completar.
    if(r.length % 8 != 0)
    {
        for(int i = 0; i < 8 - (r.length % 8); i++)
        {
            [zero appendString:@"0"];
        }
    }
    
    r = [NSString stringWithFormat:@"%@%@", zero, r];
    
    NSMutableData *d = [[NSMutableData alloc] init];
    for(int i = 0; i < r.length; i += 8)
    {
        NSRange range = NSMakeRange(i, 8);
        NSString *bNumber = [r substringWithRange:range];
        
        long result = strtol(bNumber.UTF8String, NULL, 2);
        
        [d appendBytes:&result length:1];
        
    }
    
    NSData *resultado = [NSData dataWithData:d];
    
    return resultado;
}


/*!
 @method hexval:
 @abstract Função que transforma um NSData em um NSString
 @return Retorna uma string com valores em hexadecimal
 */
+ (NSString *)hexval:(NSData *)data
{
    NSMutableString *hex = [NSMutableString string];
    unsigned char *bytes = (unsigned char *)[data bytes];
    char temp[3];
    int i = 0;
    
    for (i = 0; i < [data length]; i++) {
        temp[0] = temp[1] = temp[2] = 0;
        (void)sprintf(temp, "%02x", bytes[i]);
        [hex appendString:[NSString stringWithUTF8String:temp]];
    }
    
    return hex;
}

@end

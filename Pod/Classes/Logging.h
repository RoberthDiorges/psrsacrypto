//
//  Logging.h
//  CarteiraPagamento
//
//  Created by Ricardo Borelli
//  Copyright (c) 2016 PagSeguro. All rights reserved.
//

#pragma once

//works in DEBUG mode only
#ifndef __OPTIMIZE__
#define DebugLog(s, ...) NSLog(@"%s(%d): %@", __FUNCTION__, __LINE__, [NSString stringWithFormat:(s), ##__VA_ARGS__])
#else
#define DebugLog(s, ...)
#endif


//
//  RSAFunctions.m
//  CarteiraPagamento
//
//  Created by Rodrigo Cai on 12/10/15.
//  Copyright © 2015 e-deploy. All rights reserved.
//

#import "RSAFunctions.h"

@implementation RSAFunctions

/*
 @method convertToBinary:
 @abstract Função que transforma um número dentro de string em um número binário
 @return Retorna um número binário dentro de uma string
 */
+ (NSString *)convertToBinary:(NSString *)valor{
    NSMutableString *representacaoBinaria = [NSMutableString string];
    
    while (valor.length > 1 || ( [valor characterAtIndex:0] -48 ) > 1)
    {
        NSDictionary *dic = [RSAFunctions dividirPor2:valor];
        
        [representacaoBinaria appendString:[dic objectForKey:@"resto"]];
        
        valor = [dic objectForKey:@"resultado"];
    }
    
    if([valor characterAtIndex:0] == '1')
        [representacaoBinaria appendString:@"1"];
    
    NSString *resultado = [NSString stringWithString:representacaoBinaria];
    
    resultado = [RSAFunctions reverseString:resultado];
    
    return resultado;
}

/*!
 @method reverseString:
 @abstract Funçao que inverte os dados de uma string
 @return Retorna uma string com os valores invertidos da string de entrada
 */
+ (NSString *) reverseString:(NSString *)string
{
    NSMutableString *reversedStr;
    NSInteger len = [string length];
    
    // Auto released string
    reversedStr = [NSMutableString stringWithCapacity:len];
    
    while (len > 0){
        [reversedStr appendString:[NSString stringWithFormat:@"%C", [string characterAtIndex:--len]]];
    }
    
    return reversedStr;
}

/*!
 @method dividirPor2:
 @abstract Função que divide um valor por 2
 @return Retorna um NSDictionary com o valor da divisão na chave 'resultado' e o resto da divisão na chave 'resto'
 */
+ (NSDictionary *)dividirPor2:(NSString *)valor{
    NSMutableString *resultado = [NSMutableString string];
    NSMutableString *valorNovo = [NSMutableString string];
    
    while (valor.length > 0){
        [valorNovo setString:@""];
        
        if([valor characterAtIndex:0] == '0'){
            [resultado appendString:@"0"];
            [valorNovo appendString:[valor substringFromIndex:1]];
            
            if(valor.length > 1){
                if([valor characterAtIndex:1] == '1'){
                    [resultado appendString:@"0"];
                }
            }
        }
        else if([valor characterAtIndex:0] >= '2'){
            int result = ([valor characterAtIndex:0] - 48) / 2;
            [resultado appendString:[NSString stringWithFormat:@"%d", result]];
            
            int resto = ([valor characterAtIndex:0] - 48)  % 2;
            if(resto != 0){
                [valorNovo appendString:[NSString stringWithFormat:@"%d", resto]];
            }
            else{
                if(valor.length > 1){
                    if([valor characterAtIndex:1] == '1'){
                        [resultado appendString:@"0"];
                    }
                }
            }
            
            [valorNovo appendString:[valor substringFromIndex:1]];
        }
        else{
            if(valor.length < 2){
                NSDictionary *dic = @{@"resultado" : resultado, @"resto" : @"1"};
                return dic;
            }
            else{
                NSString *d = [NSString stringWithFormat:@"%u%u", [valor characterAtIndex:0] - 48,[valor characterAtIndex:1] - 48];
                
                int divisor = [d intValue];
                
                int result = divisor / 2;
                
                [resultado appendString:[NSString stringWithFormat:@"%d", result]];
                
                if(divisor % 2 != 0){
                    [valorNovo appendString:[NSString stringWithFormat:@"%d", divisor%2]];
                }
                else{
                    if(valor.length > 2){
                        if([valor characterAtIndex:2] == '1'){
                            [resultado appendString:@"0"];
                        }
                    }
                    
                }
                
                [valorNovo appendString:[valor substringFromIndex:2]];
            }
        }
        
        valor = [valorNovo copy];
    }
    
    NSDictionary *dicResultado = @{@"resultado" : resultado, @"resto" : @"0"};
    
    return dicResultado;
}


@end

//
//  RSAFunctions.h
//  CarteiraPagamento
//
//  Created by Rodrigo Cai on 12/10/15.
//  Copyright © 2015 e-deploy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RSAFunctions : NSObject
+ (NSString *)convertToBinary:(NSString *)valor;
+ (NSString *) reverseString:(NSString *)string;
+ (NSDictionary *)dividirPor2:(NSString *)valor;

@end

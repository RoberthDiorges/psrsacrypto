//
//  main.m
//  PSRSACrypto
//
//  Created by PSRoberthDiorges on 12/11/2018.
//  Copyright (c) 2018 PSRoberthDiorges. All rights reserved.
//

@import UIKit;
#import "PSAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PSAppDelegate class]));
    }
}

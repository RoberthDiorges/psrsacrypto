//
//  PSAppDelegate.h
//  PSRSACrypto
//
//  Created by PSRoberthDiorges on 12/11/2018.
//  Copyright (c) 2018 PSRoberthDiorges. All rights reserved.
//

@import UIKit;

@interface PSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
